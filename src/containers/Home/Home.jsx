import React from 'react';

import {
  model01Image,
  model02Image,
  lunarImage,
  flutuaImage,
  flashlightsImage,
  buildingImage,
} from '../../assets/img';

import { ContainerContent } from '../../components/elements';

import {
  GridContainer,
  FlexContainer,
} from '../../components/modules';

import * as S from './Home.styled';

const Home = () => (
  <ContainerContent>
    <GridContainer cols={2}>
      <S.TileWrapper>
        <S.HomeTile
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-end"
        >
          <S.TileHeading type="heading">
            GO MODERN
          </S.TileHeading>

          <S.TileText
            type="paragraph"
            textAlign="end"
          >
            Nada óbvios, os shapes são contemporâneos com recortes estratégicos e estampas abstratas
          </S.TileText>

          <S.TileLink>SHOP NOW</S.TileLink>
        </S.HomeTile>
      </S.TileWrapper>
      <S.TileImage src={model01Image} />
      <S.TileImage src={model02Image} />
      <S.TileWrapper>
        <S.HomeTile
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-start"
        >
          <S.TileHeading type="heading">
            CORES DA CIDADE
          </S.TileHeading>

          <S.TileText
            type="paragraph"
            textAlign="start"
          >
            Nada óbvios, os shapes são contemporâneos com recortes estratégicos e estampas abstratas
          </S.TileText>

          <S.TileLink>SHOP NOW</S.TileLink>
        </S.HomeTile>
      </S.TileWrapper>
    </GridContainer>

    <S.InsideAnimaleWrapper
      backgroundImage={buildingImage}
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
    >
      <S.TileHeading type="heading">
        ANIMALE INSIDE
      </S.TileHeading>

      <S.TileText
        type="paragraph"
        textAlign="center"
      >
        Fique por dentro do Universo Animale com apenas um clique
      </S.TileText>

      <S.TileLink>SHOP NOW</S.TileLink>
    </S.InsideAnimaleWrapper>

    <GridContainer cols={3}>
      <FlexContainer
        flexDirection="column"
        alignItems="center"
      >
        <S.TileImage src={lunarImage} />
        <S.CollectionDetails
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <S.TileHeading type="heading">
            LUNAR
          </S.TileHeading>

          <S.TileText
            type="paragraph"
            textAlign="center"
          >
            De Sevilla, capital do flamenco, o vestuário típico da dança mais celebrada
            da Espanha inspira as novas peças da coleção.
          </S.TileText>

          <S.TileLink>SHOP NOW</S.TileLink>
        </S.CollectionDetails>
      </FlexContainer>

      <FlexContainer
        flexDirection="column"
        alignItems="center"
      >
        <S.TileImage src={flutuaImage} />
        <S.CollectionDetails
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <S.TileHeading type="heading">
            FLUTUA
          </S.TileHeading>

          <S.TileText
            type="paragraph"
            textAlign="center"
          >
            A natureza dos célebres jardins espanhóis dá vida à estampas florais
          </S.TileText>

          <S.TileLink>SHOP NOW</S.TileLink>
        </S.CollectionDetails>
      </FlexContainer>

      <FlexContainer
        flexDirection="column"
        alignItems="center"
      >
        <S.TileImage src={flashlightsImage} />
        <S.CollectionDetails
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <S.TileHeading type="heading">
            FLASH LIGHTS
          </S.TileHeading>

          <S.TileText
            type="paragraph"
            textAlign="center"
          >
            O último editorial também se inspira em Barcelona, cidade iluminada pelo sol. Conheça as
            cores que são apostas da coleção.
          </S.TileText>

          <S.TileLink weight={500}>SHOP NOW</S.TileLink>
        </S.CollectionDetails>
      </FlexContainer>
    </GridContainer>
  </ContainerContent>
);

export default Home;
