import styled from 'styled-components';

import {
  Image,
  Typography,
} from '../../components/elements';

import {
  FlexContainer,
} from '../../components/modules';

const HomeTile = styled(FlexContainer)`
  height: 100%;
`;

const TileImage = styled(Image)`
  width: 100%;
`;

const TileWrapper = styled.div`
  padding: 0 8em;
  text-align: right;
  color: ${(props) => (props.theme.colors.white)};
  background: ${(props) => (props.theme.colors.black)};
`;

const TileHeading = styled(Typography)`
  margin-bottom: 0.75em;
`;

const TileText = styled(Typography)`
  width: 60%;
  font-size: 0.8rem;
  margin-bottom: 0.75em;
  text-align: ${(props) => (props.textAlign)};
`;

const TileLink = styled(Typography)`
  text-decoration: underline;
  font-size: 0.8rem;
  cursor: pointer;
`;

const InsideAnimaleWrapper = styled(FlexContainer)`
  height: 600px;
  background: linear-gradient(
      rgba(0, 0, 0, 0.5),
      rgba(0, 0, 0, 0.5)
    ), url(${(props) => (props.backgroundImage)});
  background-position: center;
  background-size: auto auto;
  background-repeat: no-repeat;
  color: ${(props) => (props.theme.colors.white)};
`;

const CollectionDetails = styled(FlexContainer)`
  width: 100%;
  padding: 3.5rem;
  background: ${(props) => (props.theme.colors.white)};
`;

export {
  HomeTile,
  TileImage,
  TileWrapper,
  TileHeading,
  TileText,
  TileLink,
  InsideAnimaleWrapper,
  CollectionDetails,
};
