import Home from './Home';
import ProductList from './ProductList';

export {
  Home,
  ProductList,
};
