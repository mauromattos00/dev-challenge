import React from 'react';
import { ThemeProvider } from 'styled-components';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
} from 'react-router-dom';

import GlobalStyle from './styles/GlobalStyle';
import theme from './styles/theme';

import {
  Header,
  Footer,
} from './components/features';

import {
  Home,
  ProductList,
} from './containers';

const App = () => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/colecao/couro" component={ProductList} />
        <Redirect to="/" />
      </Switch>
      <Footer />
    </Router>
  </ThemeProvider>
);

export default App;
