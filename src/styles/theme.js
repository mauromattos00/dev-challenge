const COLOR_WHITE = '#ffffff';
const COLOR_BLACK = '#000000';

const COLOR_GRAY10 = '#E5E5E5';
const COLOR_GRAY20 = '#7C7C7C';

const theme = {
  colors: {
    white: COLOR_WHITE,
    black: COLOR_BLACK,
    gray10: COLOR_GRAY10,
    gray20: COLOR_GRAY20,
  },
};

export default theme;
