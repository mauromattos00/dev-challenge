import styled from 'styled-components';

import {
  Typography,
  TextInput,
} from '../../elements';

import {
  FlexContainer,
  GridContainer,
} from '../../modules';

const FooterWrapper = styled(FlexContainer)`
  width: 100%;
`;

const NewsletterWrapper = styled(GridContainer)`
  width: 100%;
  padding: 2em;
  background: ${(props) => (props.theme.colors.gray10)};
`;

const NewsletterHeading = styled(FlexContainer)`
  grid-column-start: 2;
`;

const NewsletterText = styled(Typography)`
  font-size: 0.8rem;
`;

const NewsletterInputBox = styled(FlexContainer)`
  width: 430px;
  margin-bottom: 0.8rem;

  &:last-child {
    margin-bottom: 0;
  }
`;

const NewsletterInput = styled(TextInput)`
  width: ${(props) => (props.width)};
  height: 2rem;
  padding: 0.8em;
  font-size: 0.7rem;
  border: none;


`;

const NewsletterButton = styled.button`
  width: ${(props) => (props.width)};
  height: 2rem;
  padding: 0.6rem;
  color: ${(props) => (props.theme.colors.white)};
  background-color: ${(props) => (props.theme.colors.black)};
  border: none;
  font-size: 0.8rem;
  cursor: pointer;
`;

const CompanyInfoWrapper = styled(FlexContainer)`
  width: 100%;
  padding: 1rem;
  background: ${(props) => (props.theme.colors.black)};
  color: ${(props) => (props.theme.colors.white)};
`;

const CompanyInfoText = styled(Typography)`
  color: ${(props) => (props.theme.colors.white)};
  padding: 0 2rem;
  font-size: 0.8rem;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;

const CompanyLocationWrapper = styled(FlexContainer)`
  width: 100%;
  padding: 1rem 0;
  background: ${(props) => (props.theme.colors.white)};
`;

const ComponanyLocationText = styled(Typography)`
  color: ${(props) => (props.theme.colors.gray20)};
  font-size: 0.7rem;
`;

export {
  FooterWrapper,
  NewsletterWrapper,
  NewsletterHeading,
  NewsletterText,
  NewsletterInput,
  NewsletterButton,
  NewsletterInputBox,
  CompanyInfoWrapper,
  CompanyInfoText,
  CompanyLocationWrapper,
  ComponanyLocationText,
};
