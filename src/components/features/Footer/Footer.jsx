import React from 'react';

import {
  Typography,
} from '../../elements';

import {
  FlexContainer,
} from '../../modules';

import * as S from './Footer.styled';

const Footer = () => (
  <S.FooterWrapper flexDirection="column">
    <S.NewsletterWrapper cols={3}>
      <S.NewsletterHeading
        flexDirection="column"
        justifyContent="center"
      >
        <Typography type="heading">NEWSLETTER</Typography>
        <S.NewsletterText type="paragraph">
          Cadastre-se para receber nossas novidade e promoções
        </S.NewsletterText>
      </S.NewsletterHeading>
      <FlexContainer
        flexDirection="column"
      >
        <S.NewsletterInputBox>
          <S.NewsletterInput
            placeholder="NOME"
            width="100%"
          />
        </S.NewsletterInputBox>
        <S.NewsletterInputBox>
          <S.NewsletterInput
            width="65%"
            placeholder="E-MAIL"
          />
          <S.NewsletterButton width="35%">OK</S.NewsletterButton>
        </S.NewsletterInputBox>
      </FlexContainer>
    </S.NewsletterWrapper>

    <S.CompanyInfoWrapper>
      <S.CompanyInfoText weight={500}>A MARCA</S.CompanyInfoText>
      <S.CompanyInfoText weight={500}>MINHA CONTA</S.CompanyInfoText>
      <S.CompanyInfoText weight={500}>POLÍTICAS</S.CompanyInfoText>
      <S.CompanyInfoText weight={500}>FORMAS DE PAGAMENTO</S.CompanyInfoText>
      <S.CompanyInfoText weight={500}>NEWSLETTER</S.CompanyInfoText>
    </S.CompanyInfoWrapper>

    <S.CompanyLocationWrapper
      justifyContent="center"
      alignItems="center"
    >
      <S.ComponanyLocationText>
        RBX RIO COMÉRCIO DE ROUPAS LTDA. ESTRADA DOS BANDEIRANTES, 1700 - GALPÃO 03, ARMAZÉM 104
        - TAQUARA, RIO DE JANEIRO, RJ - CEP 22775-109. CNPJ 10.285.590/0002-80.
      </S.ComponanyLocationText>
    </S.CompanyLocationWrapper>
  </S.FooterWrapper>
);

export default Footer;
