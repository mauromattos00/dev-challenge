import React from 'react';
import { Link } from 'react-router-dom';

import {
  locationIcon,
  contactIcon,
  userIcon,
  shoppingCartIcon,
  searchIcon,
} from '../../../assets/icons';

import { insideImg } from '../../../assets/img';

import {
  GridContainer,
  FlexContainer,
} from '../../modules';

import * as S from './Header.styled';

const Header = () => {
  const renderNavItems = () => (
    <>
      <S.HeaderNavItem>
        NOVIDADES
      </S.HeaderNavItem>
      <S.HeaderNavItem>
        <Link to="/colecao/couro">
          COLEÇÃO
        </Link>
      </S.HeaderNavItem>
      <S.HeaderNavItem>
        JÓIAS
      </S.HeaderNavItem>
      <S.HeaderNavItem>
        SALE
      </S.HeaderNavItem>
      <S.HeaderNavItem>
        <S.HeaderNavBrandImage src={insideImg} />
      </S.HeaderNavItem>
    </>
  );

  return (
    <S.HeaderWrapper>
      <FlexContainer justifyContent="center">
        <S.Brand>
          <Link to="/">
            ANIMALE
          </Link>
        </S.Brand>
      </FlexContainer>

      <GridContainer cols={3}>
        <FlexContainer alignItems="center">
          <S.HeaderIcon src={locationIcon} />
          <S.HeaderIcon src={contactIcon} />
        </FlexContainer>

        <FlexContainer
          justifyContent="center"
          alignItems="flex-end"
        >
          {renderNavItems()}
        </FlexContainer>

        <FlexContainer justifyContent="flex-end">
          <S.HeaderIcon src={userIcon} />
          <S.HeaderIcon src={shoppingCartIcon} />
          <S.HeaderIcon src={searchIcon} />
        </FlexContainer>
      </GridContainer>
    </S.HeaderWrapper>
  );
};

export default Header;
