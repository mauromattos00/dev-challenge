import styled from 'styled-components';

import { Image } from '../../elements';

const HeaderWrapper = styled.header`
  display: flex;
  flex-direction: column;
  padding: 0 16px;
  background-color: #FFFFFF;
`;

const HeaderIcon = styled.img`
  height: 1.1em;
  margin: 16px 8px;
  cursor: pointer;
`;

const Brand = styled.span`
  padding: 16px;
  font-size: 1.2em;
`;

const HeaderNavItems = styled.div`
  display: flex;
`;

const HeaderNavItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px 16px 16px;
  font-size: 0.8em;
  transition: all 0.1s ease-out;
  cursor: pointer;

  &:hover {
    border-bottom: 2px solid ${(props) => (props.theme.colors.black)};
  }
`;

const HeaderNavBrandImage = styled(Image)`
  height: 2em;
`;

export {
  HeaderWrapper,
  HeaderIcon,
  Brand,
  HeaderNavItems,
  HeaderNavItem,
  HeaderNavBrandImage,
};
