import React from 'react';
import PropTypes from 'prop-types';

import FlexContainerWrapper from './FlexContainer.styled';

const FlexContainer = ({
  flexDirection,
  justifyContent,
  alignItems,
  children,
  ...rest
}) => (
  <FlexContainerWrapper
    flexDirection={flexDirection}
    justifyContent={justifyContent}
    alignItems={alignItems}
    {...rest}
  >
    {children}
  </FlexContainerWrapper>
);

FlexContainer.propTypes = {
  flexDirection: PropTypes.oneOf(['row', 'column']),
  justifyContent: PropTypes.oneOf(['flex-start', 'center', 'flex-end']),
  alignItems: PropTypes.oneOf(['flex-start', 'center', 'flex-end']),
  children: PropTypes.node.isRequired,
};

FlexContainer.defaultProps = {
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
};

export default FlexContainer;
