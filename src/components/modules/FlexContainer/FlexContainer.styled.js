import styled from 'styled-components';

const FlexContainerWrapper = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.flexDirection)};
  justify-content: ${(props) => (props.justifyContent)};
  align-items: ${(props) => (props.alignItems)};
`;

export default FlexContainerWrapper;
