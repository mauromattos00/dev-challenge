import styled from 'styled-components';

const GridWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(${(props) => (props.cols)}, 1fr);
`;

export {
  // eslint-disable-next-line import/prefer-default-export
  GridWrapper,
};
