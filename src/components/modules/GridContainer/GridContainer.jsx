import React from 'react';
import PropTypes from 'prop-types';

import * as S from './GridContainer.styled';

const GridContainer = ({
  cols,
  children,
  ...rest
}) => (
  <S.GridWrapper cols={cols} {...rest}>
    {children}
  </S.GridWrapper>
);

GridContainer.propTypes = {
  cols: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

export default GridContainer;
