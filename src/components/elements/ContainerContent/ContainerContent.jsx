import React from 'react';
import PropTypes from 'prop-types';

import * as S from './ContainerContent.styled';

const ContainerContent = ({ children }) => (
  <S.ContainerWrapper>
    {children}
  </S.ContainerWrapper>
);

ContainerContent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ContainerContent;
