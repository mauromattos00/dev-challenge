import React from 'react';
import PropTypes from 'prop-types';

import Typographies from './Typography.styled';

const renderComponent = (type, children, weight, props) => {
  const Component = Typographies[type];

  return (
    <Component weight={weight} {...props}>
      {children}
    </Component>
  );
};

const Typography = ({
  children,
  type,
  weight,
  ...rest
}) => (
  <>
    {renderComponent(type, children, weight, { ...rest })}
  </>
);

Typography.propTypes = {
  type: PropTypes.oneOf(['heading', 'paragraph']),
  weight: PropTypes.oneOf([300, 400, 500]),
  children: PropTypes.node.isRequired,
};

Typography.defaultProps = {
  type: 'paragraph',
  weight: 400,
};

export default Typography;
