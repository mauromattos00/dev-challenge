import styled, { css } from 'styled-components';

const basicProps = css`
  color: ${(props) => (props.theme.colors[props.color])};
`;

const heading = styled.h2`
  ${basicProps};
  font-weight: 400;
`;

const paragraph = styled.p`
  ${basicProps};
  font-size: 0.9em;
  ${(props) => (props.weight && css`
    font-weight: ${props.weight};
  `)}
`;

export default {
  heading,
  paragraph,
};
