import React from 'react';
import PropTypes from 'prop-types';

const Image = ({ alt, ...rest }) => (
  <img alt={alt} {...rest} />
);

Image.propTypes = {
  alt: PropTypes.string,
};

Image.defaultProps = {
  alt: '',
};

export default Image;
