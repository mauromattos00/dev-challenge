import React from 'react';

import * as S from './TextInput.styled';

const TextInput = ({ ...rest }) => (
  <S.Input type="text" {...rest} />
);

export default TextInput;
