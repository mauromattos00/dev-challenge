import React from 'react';
import ReactDOM from 'react-dom';

import App from './App.jsx';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(<App />, document.querySelector('#root'));
