import insideImg from './inside.png';
import model01Image from './model01.png';
import model02Image from './model02.png';
import buildingImage from './building.png';
import lunarImage from './lunar.png';
import flutuaImage from './flutua.png';
import flashlightsImage from './flashlights.png';

export {
  insideImg,
  model01Image,
  model02Image,
  buildingImage,
  lunarImage,
  flutuaImage,
  flashlightsImage,
};
