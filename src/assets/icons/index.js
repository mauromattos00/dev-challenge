import locationIcon from './lojas.png';
import contactIcon from './contato.png';
import searchIcon from './busca.png';
import shoppingCartIcon from './carrinho.png';
import userIcon from './login.png';

export {
  locationIcon,
  contactIcon,
  searchIcon,
  shoppingCartIcon,
  userIcon,
};
