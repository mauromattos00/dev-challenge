# SomaLabs Front End DevChallenge

Confira demonstração da aplicação clicando [aqui](https://dev-challenge-one.vercel.app/)

![Screenshot](./src/assets/img/screenshot-animale.png)

## Requisitos

Para executar a instalação, são necessários os seguintes recursos

#### NodeJS

Ecossistema onde o projeto é executado

> Versão 12 recomendada

#### Yarn ou NPM

Gerenciadores de dependências da aplicação

## Instalação

Execute o seguinte comando para a instalação das dependências da aplicação

``` bash
  yarn install
```

Uma vez que instalação das dependências esteja concluída, inicie a aplicação com o seguinte comando

``` bash
  yarn start
```

Uma janela irá abrir em seu navegador com aplicação.

## Informações Importantes

> A opção `COLEÇÃO` na barra de navegação foi linkada à página de listagem de
> produtos a nível de simulação.
